webpackJsonp([0,8],{

/***/ 688:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var common_1 = __webpack_require__(8);
var _404_component_1 = __webpack_require__(692);
var _500_component_1 = __webpack_require__(693);
var login_component_1 = __webpack_require__(694);
var register_component_1 = __webpack_require__(695);
var http_1 = __webpack_require__(38);
var pages_routing_module_1 = __webpack_require__(698);
var forms_1 = __webpack_require__(15);
var PagesModule = (function () {
    function PagesModule() {
    }
    return PagesModule;
}());
PagesModule = __decorate([
    core_1.NgModule({
        imports: [pages_routing_module_1.PagesRoutingModule, http_1.HttpModule, common_1.CommonModule, forms_1.FormsModule],
        declarations: [
            _404_component_1.p404Component,
            _500_component_1.p500Component,
            login_component_1.LoginComponent,
            register_component_1.RegisterComponent
        ]
    })
], PagesModule);
exports.PagesModule = PagesModule;
//# sourceMappingURL=G:/laragon/www/angular/src/pages.module.js.map

/***/ }),

/***/ 692:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var p404Component = (function () {
    function p404Component() {
    }
    return p404Component;
}());
p404Component = __decorate([
    core_1.Component({
        template: __webpack_require__(704)
    }),
    __metadata("design:paramtypes", [])
], p404Component);
exports.p404Component = p404Component;
//# sourceMappingURL=G:/laragon/www/angular/src/404.component.js.map

/***/ }),

/***/ 693:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var p500Component = (function () {
    function p500Component() {
    }
    return p500Component;
}());
p500Component = __decorate([
    core_1.Component({
        template: __webpack_require__(705)
    }),
    __metadata("design:paramtypes", [])
], p500Component);
exports.p500Component = p500Component;
//# sourceMappingURL=G:/laragon/www/angular/src/500.component.js.map

/***/ }),

/***/ 694:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var router_1 = __webpack_require__(14);
//import { LoginService } from './shared/login.service';
var index_1 = __webpack_require__(133);
var LoginComponent = (function () {
    function LoginComponent(loginService, route, router) {
        this.loginService = loginService;
        this.route = route;
        this.router = router;
        this.login = [];
        this.model = {};
        this.loading = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.submitLogin = function () {
        var _this = this;
        this.loading = true;
        this.loginService.login(this.model.username, this.model.password).subscribe(function (res) {
            var currentUserJson = localStorage.getItem('currentToken');
            //	let currentUserJson: any = localStorage.getItem('currentToken');
            //	var currentUserParse = JSON.parse(currentUserJson);
            //	this.parseJwt(currentUserParse.jwt);
            //	console.log(this.model);
            //	console.log(currentUserJson);
            if (currentUserJson !== "" || currentUserJson !== null || currentUserJson.lenght > 0) {
                _this.router.navigate(['camps']);
            }
            //	this.router.navigate(['camps/dashboard']);
        }, function (error) {
            //this.alertService.error(error);
            //this.loading = false;
        });
    };
    LoginComponent.prototype.parseJwt = function (token) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace('-', '+').replace('_', '/');
        var parseJwte = JSON.parse(window.atob(base64));
        localStorage.setItem('currentData', JSON.stringify(parseJwte.data));
        console.log(parseJwte);
    };
    ;
    return LoginComponent;
}());
LoginComponent = __decorate([
    core_1.Component({
        selector: 'login',
        template: __webpack_require__(706)
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof index_1.AuthenticationService !== "undefined" && index_1.AuthenticationService) === "function" && _a || Object, typeof (_b = typeof router_1.ActivatedRoute !== "undefined" && router_1.ActivatedRoute) === "function" && _b || Object, typeof (_c = typeof router_1.Router !== "undefined" && router_1.Router) === "function" && _c || Object])
], LoginComponent);
exports.LoginComponent = LoginComponent;
var _a, _b, _c;
/*
this.authenticationService.login(this.model.username, this.model.password)
.subscribe(
data => {
this.router.navigate([this.returnUrl]);
},
error => {
//this.alertService.error(error);
this.loading = false;
});
*/ 
//# sourceMappingURL=G:/laragon/www/angular/src/login.component.js.map

/***/ }),

/***/ 695:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var RegisterComponent = (function () {
    function RegisterComponent() {
    }
    return RegisterComponent;
}());
RegisterComponent = __decorate([
    core_1.Component({
        template: __webpack_require__(707)
    }),
    __metadata("design:paramtypes", [])
], RegisterComponent);
exports.RegisterComponent = RegisterComponent;
//# sourceMappingURL=G:/laragon/www/angular/src/register.component.js.map

/***/ }),

/***/ 698:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var router_1 = __webpack_require__(14);
var _404_component_1 = __webpack_require__(692);
var _500_component_1 = __webpack_require__(693);
var login_component_1 = __webpack_require__(694);
var register_component_1 = __webpack_require__(695);
var routes = [
    {
        path: '',
        data: {
            title: 'Example Pages'
        },
        children: [
            {
                path: '404',
                component: _404_component_1.p404Component,
                data: {
                    title: 'Page 404'
                }
            },
            {
                path: '500',
                component: _500_component_1.p500Component,
                data: {
                    title: 'Page 500'
                }
            },
            {
                path: 'login',
                component: login_component_1.LoginComponent,
                data: {
                    title: 'Login Page'
                }
            },
            {
                path: 'register',
                component: register_component_1.RegisterComponent,
                data: {
                    title: 'Register Page'
                }
            }
        ]
    }
];
var PagesRoutingModule = (function () {
    function PagesRoutingModule() {
    }
    return PagesRoutingModule;
}());
PagesRoutingModule = __decorate([
    core_1.NgModule({
        imports: [router_1.RouterModule.forChild(routes)],
        exports: [router_1.RouterModule]
    })
], PagesRoutingModule);
exports.PagesRoutingModule = PagesRoutingModule;
//# sourceMappingURL=G:/laragon/www/angular/src/pages-routing.module.js.map

/***/ }),

/***/ 704:
/***/ (function(module, exports) {

module.exports = "<div class=\"app flex-row align-items-center\">\n  <div class=\"container\">\n    <div class=\"row justify-content-center\">\n      <div class=\"col-md-6\">\n        <div class=\"clearfix\">\n          <h1 class=\"float-left display-3 mr-2\">404</h1>\n          <h4 class=\"pt-1\">Oops! You're lost.</h4>\n          <p class=\"text-muted\">The page you are looking for was not found.</p>\n        </div>\n        <div class=\"input-prepend input-group\">\n          <span class=\"input-group-addon\"><i class=\"fa fa-search\"></i></span>\n          <input id=\"prependedInput\" class=\"form-control\" size=\"16\" type=\"text\" placeholder=\"What are you looking for?\">\n          <span class=\"input-group-btn\">\n            <button class=\"btn btn-info\" type=\"button\">Search</button>\n          </span>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ 705:
/***/ (function(module, exports) {

module.exports = "<div class=\"app flex-row align-items-center\">\n  <div class=\"container\">\n    <div class=\"row justify-content-center\">\n      <div class=\"col-md-6\">\n        <div class=\"clearfix\">\n          <h1 class=\"float-left display-3 mr-2\">500</h1>\n          <h4 class=\"pt-1\">Houston, we have a problem!</h4>\n          <p class=\"text-muted\">The page you are looking for is temporarily unavailable.</p>\n        </div>\n        <div class=\"input-prepend input-group\">\n          <span class=\"input-group-addon\"><i class=\"fa fa-search\"></i></span>\n          <input id=\"prependedInput\" class=\"form-control\" size=\"16\" type=\"text\" placeholder=\"What are you looking for?\">\n          <span class=\"input-group-btn\">\n            <button class=\"btn btn-info\" type=\"button\">Search</button>\n          </span>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>  \n"

/***/ }),

/***/ 706:
/***/ (function(module, exports) {

module.exports = "<!-- Page container -->\n<div class=\"page-container\">\n<!-- Page content -->\n<div class=\"page-content\">\n\n\t\t\t<!-- Main content -->\n\t\t\t<div class=\"content-wrapper\">\n\n\t\t\t\t<!-- Content area -->\n\t\t\t\t<div class=\"content\">\n\n\t\t\t\t\t<!-- Simple login form -->\n\t\t\t\t\t<form name=\"form\" #f=\"ngForm\" novalidate>\n\t\t\t\t\t\t<div class=\"panel panel-body login-form\">\n\t\t\t\t\t\t\t<div class=\"text-center\">\n\t\t\t\t\t\t\t\t<div class=\"icon-object border-slate-300 text-slate-300\"><i class=\"icon-reading\"></i></div>\n\t\t\t\t\t\t\t\t<h5 class=\"content-group\">Login to your account <small class=\"display-block\">Enter your credentials below</small></h5>\n\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t<div class=\"form-group has-feedback has-feedback-left\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"Username\" name=\"username\" [(ngModel)]=\"model.username\" #username=\"ngModel\" required>\n\t\t\t\t\t\t\t\t<div class=\"form-control-feedback\">\n\t\t\t\t\t\t\t\t\t<i class=\"icon-user text-muted\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t<div class=\"form-group has-feedback has-feedback-left\">\n\t\t\t\t\t\t\t\t<input type=\"password\" class=\"form-control\" placeholder=\"Password\" name=\"password\" [(ngModel)]=\"model.password\" #password=\"ngModel\" required>\n\t\t\t\t\t\t\t\t<div class=\"form-control-feedback\">\n\t\t\t\t\t\t\t\t\t<i class=\"icon-lock2 text-muted\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t<div class=\"form-group\"> \n\t\t\t\t\t\t\t\t<button (click)=\"submitLogin()\" class=\"btn bg-pink-400 btn-block\">Sign in <i class=\"icon-circle-right2 position-right\"></i></button>\n\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t<div class=\"text-center\">\n\t\t\t\t\t\t\t\t<a href=\"login_password_recover.html\">Forgot password?</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</form>\n\t\t\t\t\t<!-- /simple login form -->\n\n\n\t\t\t\t\t<!-- Footer -->\n\t\t\t\t\t<div class=\"footer text-muted text-center\">\n\t\t\t\t\t\t&copy; 2015. <a href=\"#\">Limitless Web App Kit</a> by <a href=\"http://themeforest.net/user/Kopyov\" target=\"_blank\">Eugene Kopyov</a>\n\t\t\t\t\t</div>\n\t\t\t\t\t<!-- /footer -->\n\n\t\t\t\t</div>\n\t\t\t\t<!-- /content area -->\n\n\t\t\t</div>\n\t\t\t<!-- /main content -->\n\n</div>\n<!-- /page content -->\n\n</div>\n<!-- /page container -->"

/***/ }),

/***/ 707:
/***/ (function(module, exports) {

module.exports = "<div class=\"app flex-row align-items-center\">\n  <div class=\"container\">\n    <div class=\"row justify-content-center\">\n      <div class=\"col-md-6\">\n        <div class=\"card mx-2\">\n          <div class=\"card-block p-2\">\n            <h1>Register</h1>\n            <p class=\"text-muted\">Create your account</p>\n            <div class=\"input-group mb-1\">\n              <span class=\"input-group-addon\"><i class=\"icon-user\"></i></span>\n              <input type=\"text\" class=\"form-control\" placeholder=\"Username\">\n            </div>\n\n            <div class=\"input-group mb-1\">\n              <span class=\"input-group-addon\">@</span>\n              <input type=\"text\" class=\"form-control\" placeholder=\"Email\">\n            </div>\n\n            <div class=\"input-group mb-1\">\n              <span class=\"input-group-addon\"><i class=\"icon-lock\"></i></span>\n              <input type=\"password\" class=\"form-control\" placeholder=\"Password\">\n            </div>\n\n            <div class=\"input-group mb-2\">\n              <span class=\"input-group-addon\"><i class=\"icon-lock\"></i></span>\n              <input type=\"password\" class=\"form-control\" placeholder=\"Repeat password\">\n            </div>\n\n            <button type=\"button\" class=\"btn btn-block btn-success\">Create Account</button>\n          </div>\n          <div class=\"card-footer p-2\">\n            <div class=\"row\">\n              <div class=\"col-6\">\n                <button class=\"btn btn-block btn-facebook\" type=\"button\"><span>facebook</span></button>\n              </div>\n              <div class=\"col-6\">\n                <button class=\"btn btn-block btn-twitter\" type=\"button\"><span>twitter</span></button>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ })

});
//# sourceMappingURL=0.chunk.js.map