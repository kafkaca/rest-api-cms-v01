import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Subscriber } from 'rxjs/Subscriber';
import { staticsData } from '../shared/statics-data';


export interface IUploadOptions {
  url: string;
  method: string;
  file?: File;
  body?: any;
  headers?: { [key: string]: string };
}

@Injectable()
export class TdFileService {
  public readFile;
  private _progressSubject: Subject<number> = new Subject<number>();
  private _progressObservable: Observable<number>;
  

  /**
   * Gets progress observable to keep track of the files being uploaded.
   * Needs to be supported by backend.
   */
  get progress(): Observable<number> {

    return this._progressObservable;
  }

  constructor() {
    this._progressObservable = this._progressSubject.asObservable();
  }

  /**
   * params:
   * - options: IUploadOptions {
   *     url: string,
   *     method: 'post' | 'put',
   *     file: File,
   *     headers?: {[key: string]: string}
   * }
   *
   * Uses underlying [XMLHttpRequest] to upload a file to a url.
   * Will be depricated when angular2 fixes [Http] to allow [FormData] as body.
   */
  upload(options: IUploadOptions): Observable<any> {
    return new Observable<any>((subscriber: Subscriber<any>) => {
      let xhr: XMLHttpRequest = new XMLHttpRequest();
      let formData: FormData = new FormData();
      if (options.file) {
        formData.append('imaj', options.file);
      }
      if (options.body) {
        formData.append('body', JSON.stringify(options.body));
      }


      xhr.onprogress = (event: ProgressEvent) => {
        let progress: number = 0;
        if (event.total > 0) {
          progress = Math.round(event.loaded / event.total * 100);
        }


        this._progressSubject.next(progress);
      };

      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200 || xhr.status === 201) {
            subscriber.next(JSON.parse(xhr.response));
            subscriber.complete();
          } else {
            subscriber.error(xhr.response);
          }
        }
      };

      xhr.open(options.method, staticsData.api_url + options.url, true);
      xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      if (options.headers) {
        for (let key in options.headers) {
          xhr.setRequestHeader(key, options.headers[key]);
        }
      }

      xhr.send(formData);
    });
  }

  readData(file : any) : Observable<any> {
  
     return new Observable<any>((subscriber: Subscriber<any>) => {
    var reader = new FileReader();
    reader.onload = () => {
       var image = new Image();
   image.onload = function (imageEvent) {
                // Resize the image
                var canvas = document.createElement('canvas'),
                    max_size = 1024,// TODO : pull max size from a site config
                    width = image.width,
                    height = image.height;

                canvas.width = width;
                canvas.height = height;
                canvas.getContext('2d').drawImage(image, 0, 0, width, height);
                var dataUrl = canvas.toDataURL('image/jpeg');
               // console.log(dataUrl);
                 subscriber.next(dataUrl);
                  subscriber.complete();
   }
   image.src = reader.result;
   //console.log(image);
   
           
           
    }
    reader.readAsDataURL(file);
     })
  }

resizeImage(url, width, height, callback) {
             /*    if (width > height) {
                    if (width > max_size) {
                        height *= max_size / width;
                        width = max_size;
                    }
                } else {
                    if (height > max_size) {
                        width *= max_size / height;
                        height = max_size;
                    }
                }*/
    var sourceImage = new Image();

    sourceImage.onload = function() {
        // Create a canvas with the desired dimensions
        var canvas = document.createElement("canvas");
        canvas.width = width;
        canvas.height = height;

        // Scale and draw the source image to the canvas
        canvas.getContext("2d").drawImage(sourceImage, 0, 0, width, height);

        // Convert the canvas to a data URL in PNG format
        callback(canvas.toDataURL());
    }

    sourceImage.src = url;
}


}

/*
var options = {
url: "/demo-return",
method: "post",
file: event.target.files[0]
}
this.formData = options;
this.formData['body'] = f.value;
this.uploadService.upload(this.formData).subscribe(resp => {
console.log(resp);
},
error => {
console.log(error);
});
*/