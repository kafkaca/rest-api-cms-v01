import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response, RequestMethod } from '@angular/http';
import { staticsData } from '../shared/statics-data';
import { Admin } from '../_models/index';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subscriber } from 'rxjs/Subscriber';
//var formDemo = "email=demo@testmail.com&password=123456";
@Injectable()
export class AdmService {
    constructor(private http: Http, private router: Router) { }
    createAds(formData) {
        return this.http.post(staticsData.api_url + '/reklam-ekle', formData).map((response: Response) => response.json());
    }
    createAdvert(formData) {
        return this.http.post(staticsData.api_url + '/add-advert', formData).map((response: Response) => response.json());
    }
    updateAdvert(formData) {
        return this.http.post(staticsData.api_url + '/update-advert', formData).map((response: Response) => response.json());
    }
    createCampaign(formData) {
        return this.http.post(staticsData.api_url + '/add-campaign', formData).map((response: Response) => response.json());
    }
    updateRules(formData) {
        return this.http.post(staticsData.api_url + '/update-rules', formData).map((response: Response) => response.json());
    }
    updateCampaign(formData) {
        return this.http.post(staticsData.api_url + '/update-campaign', formData).map((response: Response) => response.json());
    }

    getAdItem(camp_id: number, ad_id: number) {
        return this.http.get(staticsData.api_url + `/camp/${camp_id}/${ad_id}`).map((response: Response) => response.json());
    }
    getAllItems(user_id, current_page) {
        return this.http.get(staticsData.api_url + `/ads/${user_id}?page=` + current_page).map((response: Response) => response.json());
    }
    getCampItems(camp_id, current_page) {
        return this.http.get(staticsData.api_url + `/cads/${camp_id}?page=` + current_page).map((response: Response) => response.json());
    }
    getCampaign(camp_id: number) {
        return this.http.get(staticsData.api_url + '/camp/' + camp_id).map((response: Response) => response.json());
    }
    getAllCampaigns(current_Page) {
        return this.http.get(staticsData.api_url + '/tum-kampanyalar?page=' + current_Page).map((response: Response) => response.json());
    }

    getById(id: number) {
        return this.http.get('/api/users/' + id, this.jwt()).map((response: Response) => response.json());
    }

    create() {
        return this.http.post(staticsData.api_url + '/user/checkRequestBodyWithoutToken', {}, this.jwt()).map((response: Response) => response.json());
    }

    update(user: Admin) {
        return this.http.put('/api/users/' + user.id, user, this.jwt()).map((response: Response) => response.json());
    }

    delete(id: number) {
        return this.http.delete('/api/users/' + id, this.jwt()).map((response: Response) => response.json());
    }
    afterSubmitRedirect(camp_id: number) {
        this.router.navigate(['/camps', camp_id]);
    }


    getPaginationData = (collect :any) => {
        let PaginationModel = {
            current_Page: collect.current_page,
            totalCampaign: collect.total,
            next_Page: collect.next_page_url,
            prev_Page: collect.prev_page_url,
            per_Page: collect.per_page,
            numPages: collect.current_page 
        }

     return PaginationModel;



    }
    private jwt() {
        let currentUserParse = JSON.parse(localStorage.getItem('currentToken'));
        if (currentUserParse) {
            if (currentUserParse.jwt) {
                let headers = new Headers({
                    "Authorization": currentUserParse.jwt,
                    'Content-Type': 'application/json; charset=UTF-8'
                });
                return new RequestOptions({ headers: headers });
            }
        }

    }
}