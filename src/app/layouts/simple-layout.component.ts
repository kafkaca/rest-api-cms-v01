import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart} from '@angular/router';


declare var window : any;
@Component({
    selector: 'simple-layout',
  templateUrl: './simple-layout.component.html'
})
export class SimpleLayoutComponent implements OnInit {
  constructor(private router:Router) {
  	this.router.events.subscribe((event) => {
  if(event instanceof NavigationStart) {
  window.require('/assets/js/core/app.js', window.callback);
  }});
  };
    ngOnInit() {}
}
