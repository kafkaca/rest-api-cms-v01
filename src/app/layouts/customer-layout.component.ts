import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../_services/index';
@Component({
  selector: 'app-dashboard',
  templateUrl: './customer-layout.component.html'
})
export class CustomerLayoutComponent implements OnInit {
  constructor(private loginService: AuthenticationService) { };
  ngOnInit() { }

  onLogout(){
this.loginService.logout();
  }

}
