import { ActionReducer, Action } from '@ngrx/store';

export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const RESET = 'RESET';
export const GUNCELLE = 'GUNCELLE';

let allState = {
	counter: 0,
	datalar: '',
	menu: {}
}

export function counterReducer(state: any, action: Action) {
	console.log(action);

	switch (action.type) {
		case INCREMENT:
			return state + 1;
		case DECREMENT:
			return state - 1;
		case "ANKARA":
			return "ISTANBUL";
		case RESET:
			return 0;
		default:
			return allState;
	}
}