import { TestBed, inject } from '@angular/core/testing';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

import { SidebarComponent } from './sidebar.component';
import { SidebarService } from './shared/sidebar.service';
import { Sidebar } from './shared/sidebar.model';

describe('a sidebar component', () => {
	let component: SidebarComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [HttpModule],
			providers: [
				{ provide: SidebarService, useClass: MockSidebarService },
				SidebarComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([SidebarComponent], (SidebarComponent) => {
		component = SidebarComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});

// Mock of the original sidebar service
class MockSidebarService extends SidebarService {
	getList(): Observable<any> {
		return Observable.from([ { id: 1, name: 'One'}, { id: 2, name: 'Two'} ]);
	}
}
