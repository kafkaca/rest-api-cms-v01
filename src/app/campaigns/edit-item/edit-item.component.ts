import { Component, OnInit, OnDestroy } from '@angular/core';
import { AdmService } from '../../_services/adm.service';
import { UploadServiceService } from '../../_services/upload-service.service';
import { TdFileService } from '../../_services/file.service';
import { Router, ActivatedRoute } from '@angular/router';
import { staticsData } from '../../shared/statics-data';
@Component({
	selector: 'edit-item',
	templateUrl: 'edit-item.component.html'
})
export class EditItemComponent implements OnInit {
	ad_item: any
	camp_id: number
	ad_id: number
	cikisKodu = ``;
	constructor(private uploadService: TdFileService, private adminService: AdmService, private route: ActivatedRoute) {
		this.camp_id = route.snapshot.params['camp_id'];
		this.ad_id = route.snapshot.params['ad_id'];
		//console.log(route.snapshot.params);
	}
	ngOnInit() {
		this.adminService.getAdItem(this.camp_id, this.ad_id).subscribe(resp => {
			this.ad_item = resp.ad;
			//console.log(staticsData.currentUser);
			if (this.camp_id) {
				this.cikisKodu = `<script data-info="${this.ad_item.camp_id}${this.ad_item.id},${this.ad_item.user_id},${this.ad_item.camp_id},${this.ad_item.id}" src="//www.liberyen.com/js/init_ads.js"></script>`
			}
			console.log(resp);
		},
			error => {
				console.log(error);
			});
		document.body.classList.add("sidebar-xs")
	}
	ngOnDestroy() {
		document.body.classList.remove("sidebar-xs")
	}

}