import { TestBed, inject } from '@angular/core/testing';

import { EditItemComponent } from './edit-item.component';

describe('a edit-item component', () => {
	let component: EditItemComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				EditItemComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([EditItemComponent], (EditItemComponent) => {
		component = EditItemComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});