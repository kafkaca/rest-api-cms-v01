import { TestBed, inject } from '@angular/core/testing';

import { EditCampaignComponent } from './edit-campaign.component';

describe('a edit-campaign component', () => {
	let component: EditCampaignComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				EditCampaignComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([EditCampaignComponent], (EditCampaignComponent) => {
		component = EditCampaignComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});