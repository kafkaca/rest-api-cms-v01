import { TestBed, inject } from '@angular/core/testing';

import { AddItemComponent } from './add-item.component';

describe('a add-item component', () => {
	let component: AddItemComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				AddItemComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([AddItemComponent], (AddItemComponent) => {
		component = AddItemComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});