import { TestBed, inject } from '@angular/core/testing';

import { AddCampaignComponent } from './add-campaign.component';

describe('a add-campaign component', () => {
	let component: AddCampaignComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				AddCampaignComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([AddCampaignComponent], (AddCampaignComponent) => {
		component = AddCampaignComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});