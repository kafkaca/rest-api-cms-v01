import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CampaignsComponent } from './campaigns.component';
import { AddCampaignComponent } from './add-campaign/add-campaign.component';
import { AddItemComponent } from './add-item/add-item.component';
import { EditCampaignComponent } from './edit-campaign/edit-campaign.component';
import { EditItemComponent } from './edit-item/edit-item.component';
import { AllAdsComponent } from './all-ads/all-ads.component';
import { EmptyComponent } from './empty/empty.component';
const routes: Routes = [
  {
    path: '',
    component: CampaignsComponent,
    data: {
      title: 'Kampanyalar'
    }
  },
  {
    path: 'add-camp',
    component: AddCampaignComponent,
    data: {
      title: 'Add Campaign'
    }
  },
  {
    path: 'add-item',
    component: AddItemComponent,
    data: {
      title: 'All Ads'
    }
  },
  {
    path: 'all-ads',
    component: AllAdsComponent,
    data: {
      title: 'Add Item'
    }
  },
  {
    path: ':camp_id',
    children: [
      {
        path: '',
        component: EditCampaignComponent,
        data: {
          title: 'Edit Campaign'
        }
      },
      {
        path: 'add-item',
        component: AddItemComponent,
        data: {
          title: 'Add Item'
        }
      },
      {
        path: 'edit-item/:ad_id',
        component: EditItemComponent,
        data: {
          title: 'Edit Item'
        }
      }
    ]
  },
  {
    path: 'manage',
    children: [
      {
        path: 'reports',
        component: EmptyComponent,
        data: {
          title: 'Edit Campaign'
        }
      },
      {
        path: 'sites',
        component: EmptyComponent,
        data: {
          title: 'Add Item'
        }
      },
      {
        path: 'agency',
        component: EmptyComponent,
        data: {
          title: 'Edit Item'
        }
      },
      {
        path: 'empty',
        component: EmptyComponent,
        data: {
          title: 'Edit Item'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CampaignsRoutingModule { }


