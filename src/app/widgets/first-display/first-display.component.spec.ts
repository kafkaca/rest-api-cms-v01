import { TestBed, inject } from '@angular/core/testing';

import { FirstDisplayComponent } from './first-display.component';

describe('a first-display component', () => {
	let component: FirstDisplayComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				FirstDisplayComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([FirstDisplayComponent], (FirstDisplayComponent) => {
		component = FirstDisplayComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});