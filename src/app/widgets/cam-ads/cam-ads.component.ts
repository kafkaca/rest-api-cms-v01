import { Component, Input, OnInit } from '@angular/core';

@Component({
	selector: 'cam-ads',
	templateUrl: 'cam-ads.component.html'
})

export class CamAdsComponent implements OnInit {
	@Input("cam_ads") cam_ads: any;
	ngOnInit() { }
}