import { TestBed, inject } from '@angular/core/testing';

import { CampToolsComponent } from './camp-tools.component';

describe('a camp-tools component', () => {
	let component: CampToolsComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				CampToolsComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([CampToolsComponent], (CampToolsComponent) => {
		component = CampToolsComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});