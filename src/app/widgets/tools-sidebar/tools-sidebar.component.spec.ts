import { TestBed, inject } from '@angular/core/testing';

import { ToolsSidebarComponent } from './tools-sidebar.component';

describe('a tools-sidebar component', () => {
	let component: ToolsSidebarComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				ToolsSidebarComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([ToolsSidebarComponent], (ToolsSidebarComponent) => {
		component = ToolsSidebarComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});