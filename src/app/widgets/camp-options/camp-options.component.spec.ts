import { TestBed, inject } from '@angular/core/testing';

import { CampOptionsComponent } from './camp-options.component';

describe('a camp-options component', () => {
	let component: CampOptionsComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				CampOptionsComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([CampOptionsComponent], (CampOptionsComponent) => {
		component = CampOptionsComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});