import { Component, OnInit } from '@angular/core';
import { AdmService } from '../_services/adm.service';
@Component({
  selector: 'app-administrator',
  templateUrl: './administrator.component.html',
  styleUrls: ['./administrator.component.css'],
  providers: [AdmService]
})
export class AdministratorComponent implements OnInit {
constructor(private adminService : AdmService) { }
  ngOnInit() {}
}