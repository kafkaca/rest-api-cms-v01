import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { AdministratorComponent } from './administrator.component';
import { AdministratorRoutingModule } from './administrator.routing';
import { SettingsComponent } from './settings/settings.component';
@NgModule({
  imports: [
    HttpModule,
    CommonModule,
    AdministratorRoutingModule
  ],
  declarations: [AdministratorComponent, SettingsComponent]
})
export class AdministratorModule { }
