import { TestBed, inject } from '@angular/core/testing';

import { TextImageAdComponent } from './text-image-ad.component';

describe('a text-image-ad component', () => {
	let component: TextImageAdComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				TextImageAdComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([TextImageAdComponent], (TextImageAdComponent) => {
		component = TextImageAdComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});