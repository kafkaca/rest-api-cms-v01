import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { AdmService } from '../../_services/adm.service';
import { TdFileService } from '../../_services/file.service';
declare var window: any;
declare var $: any;
@Component({
	selector: 'text-image-ad',
	templateUrl: 'text-image-ad.component.html'
})

export class TextImageAdComponent implements OnInit, AfterViewInit {

	constructor(private adminService: AdmService, private uploadService: TdFileService) { }
	currentForm = {};
	uploadedFile: string = "";
	imageFolder = "http://www.liberyen.com/uploads/images/";
	currentImage: string;
	defaultImage = "default.jpg"
	cikisKodu : string = ``;
	@Input("camp_id") camp_id: number;
	@Input("ad_data") ad_data: any = {};

	ngOnInit() {

				this.currentForm = this.ad_data

	}

	ngAfterViewInit() {
		setTimeout(() => {
			if (this.ad_data.id) {
this.currentImage = this.imageFolder + this.ad_data.ad_body.image
			}
			else{
this.currentImage = this.imageFolder + this.defaultImage
			}
				// Default file input style
	$(".file-styled").uniform({
		fileButtonClass: 'action btn bg-blue',
		fileButtonHtml : 'Bir Dosya Seçin',
		fileDefaultHtml : "Seçilmedi"
	});
		}, 300);
		


	}


	formState(event) {
		if (event.target.type === 'file') {
			this.uploadService.readData(event.target.files[0]).subscribe(resp => {
				this.currentImage = resp;
			}, error => {
				console.log(error);
			});
		}

}

	uploadTest(event) {
		console.log(event.target.files);
			var options = {
				url: "/demo-return",
				method: "post",
				file: event.target.files[0]
			}
			this.uploadService.upload(options).subscribe(resp => {
				this.uploadedFile = resp;
			}, error => {
				console.log(error);
			});

	}
	submitAd(textImage : any) {
		if (this.uploadedFile) {
			textImage.value.ad_body['image'] = this.uploadedFile;
		}
	if (this.ad_data.id) {
			this.adminService.updateAdvert(textImage.value).subscribe(resp => {
				//console.log(resp);
			}, error => {
				console.log(error);
			});
		} else {
			this.adminService.createAdvert(textImage.value).subscribe(resp => {
				//console.log(resp);
			}, error => {
				console.log(error);
			});
		}

	}

}

/*
//this.adObjects.push(this.adModel)
//this.adObjects[this.set_Slider].baslik = event.target.value
//this.adObjects[this.set_Slider].deneme = event.target.value
//console.log(this.adObjects[this.set_Slider])
//delete textImage.value["cikis_url"];
//textImage.value['ad_body'] = "";
//console.log(f.value);
//console.log(this.uploadedFile);
//console.log(resp);
//this.adminService.afterSubmitRedirect(1)
  {
  "name" : "bir reklam adı",
  "ad_body" : "",
  "note" : "",
  "type" : "banner",
  "camp_id" : 1,
  "rule_id" : 1,
  "user_id" : 1,
  "count_id" : 1,
  "status" : 0
}
*/