import { Component, OnInit, Input, AfterViewInit, OnDestroy, ChangeDetectionStrategy, Renderer2, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AdmService } from '../../_services/adm.service';
import { TdFileService } from '../../_services/file.service';
import { Observable } from 'rxjs/Observable';
declare var window: any;
declare var $: any;
@Component({
	selector: 'image-ad',
	templateUrl: './image-ad.component.html',
	styleUrls: ['./image-ad.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageAdComponent implements OnInit, AfterViewInit, OnDestroy {

	formData: any;
	uploadedFile: string = "/assets/images/placeholder.jpg";
	switch_expression: string = 'default2';
	currentImage: string = "/assets/images/placeholder.jpg";
	adModel = { "name": "bir baslik", "image": this.currentImage, "note": "bir açıklama" };
	adObjects = []
	set_Slider: number;
	 @ViewChild('uploadForm') uploadForm;
	constructor(private adminService: AdmService, private uploadService: TdFileService, private renderer: Renderer2) { }

	@Input("camp_id") camp_id: number;

	ngAfterViewInit() {
		console.log('after view');
	/*	    let simple = this.renderer.listen(this.uploadForm.nativeElement, 'change', (evt) => {
				evt.stopPropagation();
				this.fileChangeEvent(evt)
      console.log('Clicking the button', evt);
    });
*/

setTimeout(function() {
		// Default file input style
	$(".file-styled").uniform({
		fileButtonClass: 'action btn bg-blue',
		fileButtonHtml : 'Bir Dosya Seçin',
		fileDefaultHtml : "Seçilmedi"
	});
}, 300);


	}
	ngOnInit() {

    //simple();
this.adObjects.push(Object.assign({}, this.adModel))
this.set_Slider = 0;
	//	console.log('on init');
		
	}

	ngOnDestroy() {

		//console.log('on destroy');
	}
	addSlider() {
		this.adObjects.push(Object.assign({}, this.adModel))
		this.set_Slider = this.adObjects.length -1;
	}

	setSlider(index: any) {
		//console.log(index);
		this.set_Slider = index;
		//document.getElementById("slide_" + index).style.border = "3px solid black";
	}
	removeSlider(num: number) {
		this.adObjects.splice(num, 1);
	}

	formState(event : any){
	event.stopPropagation();
	//event.preeventDefault();
		console.log("basılıyor");
		if (event.target.type === 'file') {
			var options = {
				url: "/demo-return",
				method: "post",
				file: event.target.files[0]
			}

			this.uploadService.readData(event.target.files[0]).subscribe(resp => {
			this.adObjects[this.set_Slider]["image"] = resp;
		document.getElementById("slide_image_" + this.set_Slider).setAttribute( 'src', resp);

			}, error => {
				console.log(error);
			});
		/*	this.uploadService.upload(options).subscribe(resp => {
				this.uploadedFile = resp;
			}, error => {
				console.log(error);
			});*/
		}
		else {
			this.adObjects[this.set_Slider][event.target.name] = event.target.value
		}

//this.adObjects = this.adObjects;
	}

fileChangeEvent = (fileInput: any) => {
fileInput.stopPropagation();
      if (fileInput.target.files && fileInput.target.files[0]) {
        var reader = new FileReader();

        reader.onload = (e : any) => {
			this.adObjects[this.set_Slider]["image"] = e.target.result
			//return e.target.result
        }
        reader.readAsDataURL(fileInput.target.files[0]);
    }
}
gonder(){

}

	submitAd(f: NgForm) {
		if (this.uploadedFile) {
			f.value['current_image'] = this.uploadedFile;
		}
		this.adminService.createAds(f.value).subscribe(resp => {
			this.adminService.afterSubmitRedirect(1);
		}, error => {
			console.log(error);
		});


	}
	denemeYap(event: any) {
		//event.target.style.transform = "rotate(20deg)";
		var x = event.clientX;
		var y = event.clientY;
		var coor = "X coords: " + x + ", Y coords: " + y;
		var z = document.getElementById('burasibir');
		var d = document.getElementById('burasiiki');
		var lo = z.getBoundingClientRect();
		d.style.position = "fixed";
		d.style.left = lo.left + lo.width + 20 + 'px';
		d.style.top = y + 'px';
		//$('#burasibir').append(`<div class='foo'>Hello world!</div>`);
		console.log(coor);
		console.log(lo);
	}
}
/*
//this.adObjects.push(this.adModel)
//document.getElementById("slide_" + this.set_Slider)
//console.log(e.target.result);
//this.adObjects[this.set_Slider].deneme = event.target.value
//console.log(this.adObjects[this.set_Slider])
//(keyup)="adObjects[set_Slider].name = ifForm.value.name"
//console.log(f.value);
//console.log(this.uploadedFile);
*/
//source
//https://stackoverflow.com/questions/36056628/angular2-mouse-event-handling-movement-relative-to-current-position