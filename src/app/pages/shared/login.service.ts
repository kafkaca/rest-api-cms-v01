import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

import { Login } from './login.model';



@Injectable()
export class LoginService {

    constructor(private http: Http) { }

    getList(): Observable<Login[]> {
        return this.http.get('/api/list').map(res => res.json() as Login[]);
    }

    login(username: string, password: string) {
        let data = "email=" + username + "&password=" + password;
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post('//teniscim.herokuapp.com/user/login', data, options)
            .map((response: Response) => {
                console.log(response);
                let user = response.json();
                if (user) {

                    localStorage.setItem('currentUser', JSON.stringify(user));
                }
                return user;
            });
    }


    getLogin() {
        var data_old = JSON.stringify({
            email: "suat@testmail.com",
            password: "123"
        });
        let headers = new Headers({ 'Content-Type': 'application/json' });
        return this.http.post('http://teniscim.herokuapp.com/user/login', { email: "suat@testmail.com", password: "123" })
            .map((response: Response) => {
                console.log(response.json());
            });
    }

    logout() {
        // kullanıcı bilgisini siler
        localStorage.removeItem('currentUser');
    }
}