import { Ang01Page } from './app.po';

describe('ang01 App', () => {
  let page: Ang01Page;

  beforeEach(() => {
    page = new Ang01Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
